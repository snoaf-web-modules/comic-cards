var swipe_det = {
    startX: 0,
    startY: 0,
    endX: 0,
    endY: 0
};

const swipe_settings = {
    min_x: 30,  //min x swipe for horizontal swipe
    max_x: 30,  //max x difference for vertical swipe
    min_y: 50,  //min y swipe for vertical swipe
    max_y: 60   //max y difference for horizontal swipe
};

function getSwipeStart(e){
    if (e.touches.length == 1)
    {
        var t = e.touches[0];
        swipe_det.startX = t.screenX;
        swipe_det.startY = t.screenY;
    }
}

function updateSwipeEnd(e){
    // e.preventDefault();
    var t = e.touches[0];
    swipe_det.endX = t.screenX;
    swipe_det.endY = t.screenY;
}

function calculateSwipe (callback) {
    return function (e) {
        var direction = '';

        var absDistX = Math.abs(swipe_det.endX - swipe_det.startX);
        var absDistY = Math.abs(swipe_det.endY - swipe_det.startY);

        //horizontal detection
        if ((absDistX > swipe_settings.min_x) && (absDistY < swipe_settings.max_y) && (swipe_det.endX > 0))
        {
            direction = (swipe_det.endX > swipe_det.startX) ? 'right' : 'left';
        }
        //vertical detection
        else if ((absDistY > swipe_settings.min_y) && (absDistX < swipe_settings.max_x) && (swipe_det.endY > 0))
        {
            direction = (swipe_det.endY > swipe_det.startY) ? 'down' : 'up';
        }

        if (direction != '') {
            if(typeof callback == 'function')
            {
                callback(direction);
            }
        }

        swipe_det = {
            startX: 0,
            startY: 0,
            endX: 0,
            endY: 0
        };
    }
}

// function to detect swipe actions here
function addSwipeListener(elem, callback) {
    elem.addEventListener('touchstart', getSwipeStart, false);
    elem.addEventListener('touchmove', updateSwipeEnd, false);
    elem.addEventListener('touchend', calculateSwipe(callback), false);
}

function removeSwipeListener(elem, callback) {
    elem.removeEventListener('touchstart', getSwipeStart, false);
    elem.removeEventListener('touchmove', updateSwipeEnd, false);
    elem.removeEventListener('touchend', calculateSwipe(callback), false);
}

function handler(element, direction) {
    console.log("you swiped on element with id '", element, "' to ", direction, " direction");
}

// addSwipeListener(document.getElementsByClassName('comic-card')[0], handler);