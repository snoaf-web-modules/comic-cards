Vue.config.productionTip = false;

var vm = new Vue({
    el: '.app',
    data: {
        text: true,
        navArrows: true,
    },
    computed: {

    },
    created() {

    },
    methods: {
        toggleText() {
            this.text = !this.text;
        },
        toggleNavArrows() {
            this.navArrows = !this.navArrows
        }
    }
});

