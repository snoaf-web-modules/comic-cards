'use strict';

let path = require('path');
let core = require('core-libs');

let bootstrap   = core.bootstrap;
let renderEngine    = core.renderEngine;

let ComicsRenderCtx = require('./context/ComicsRenderContext').factory;

class ComicsRenderer {

    init() {
        this.pages = {
            '/': path.join('cards.html')
        };

        return Promise.resolve();
    }

    getPage(req, res, next) {
        if (this.pages.hasOwnProperty(req.url))
        {
            let filePath = renderEngine.getPagePath('comic-cards', this.pages[req.url]);
            res.render(filePath, ComicsRenderCtx.init(req));
        }
        else
        {
            next();
        }
    }
}

let _singleton = new ComicsRenderer();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;