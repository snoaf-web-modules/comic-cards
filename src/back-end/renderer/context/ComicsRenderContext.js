'use strict';

let merge = require('deepmerge');
let core = require('core-libs');
let CoreContext = core.context;

class ComicsRenderContextFactory {

    constructor() {}

    init(req) {
        let selectorCtx = new ComicsRenderContextMixin(req);
        let merged = merge(req.ctx, selectorCtx);

        req.ctx = merged;
        return merged;
    }
}

class ComicsRenderContextMixin extends CoreContext {

    constructor(req) {
        super(req);
    }
}

module.exports.factory = new ComicsRenderContextFactory();
module.exports.mixin = ComicsRenderContextMixin;