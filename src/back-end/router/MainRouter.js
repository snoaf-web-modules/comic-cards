'use strict';

let express = require('express');
let core = require('core-libs');

let bootstrap   = core.bootstrap;
let logger      = core.loggerFactory.getLogger(__filename);
let CoreRouter      = core.router;
let renderEngine    = core.renderEngine;

let htmlRouter      = require('./HtmlRouter');

class StoriesMainRouter extends CoreRouter {
    init() {
        return super.init('comic-cards')
            .then(() => {
                this.install();
                return Promise.resolve();
            })
            .catch((err) => {
                logger.error('Could setup Router, err:', err);
                return Promise.reject(err);
            });
    }

    install() {
        this.app.engine('html', renderEngine.getHtmlEngine);
        this.app.engine('bundle', renderEngine.getBundleEngine);

        // Install routes (order important)
        this.app
            .use('/static/js',       express.static(renderEngine.getStaticPath('comic-cards', 'js')))
            .use('/static/css',      express.static(renderEngine.getStaticPath('comic-cards', 'css')))
            .use('/static/fonts',    express.static(renderEngine.getStaticPath('comic-cards', 'fonts')))
            .use('/static/images',   express.static(renderEngine.getImagePath('comic-cards')))

            .use('/', htmlRouter.getRouter())
        ;
    }
}

let _singleton = new StoriesMainRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;